package sheridan;

public class LoginValidator {

	private static int MIN_LENGTH = 6;
	
	public static boolean isValidLoginName( String loginName ) {
		boolean validLength = (loginName != null && loginName.length() >= MIN_LENGTH);
		boolean validCharacters = false;
		
		if (loginName  != null && !Character.isDigit(loginName.charAt(0))) {
			validCharacters = loginName.chars().allMatch(i -> Character.isLetterOrDigit(i));
		}
		
		return validLength && validCharacters;
	}
}
