package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	// Tests for login length
	@Test
	public void testIsValidLogin_Length_Regular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLogin_Length_Exceptional() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName(null));
	}
	
	@Test
	public void testIsValidLogin_Length_BoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("brian1"));
	}
	
	@Test
	public void testIsValidLogin_Length_BoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("homer"));
	}
	
	// Tests for character requirements
	@Test
	public void testIsValidLogin_Characters_Regular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("tonystark"));
	}
	
	@Test
	public void testIsValidLogin_Characters_Exceptional() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("login!?"));
	}
	
	@Test
	public void testIsValidLogin_Characters_BoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("bbanner99"));
	}
	
	@Test
	public void testIsValidLogin_Characters_BoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("9bbanner9"));
	}

}
